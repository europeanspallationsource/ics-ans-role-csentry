import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_csentry_containers(host):
    with host.sudo():
        cmd = host.command('docker ps')
    assert cmd.rc == 0
    # Get the names of the running containers
    # - skip the first line (header)
    # - take the last element of the remaining lines
    names = sorted([line.split()[-1] for line in cmd.stdout.split('\n')[1:]])
    assert names == ['csentry_postgres', 'csentry_redis', 'csentry_web', 'traefik_proxy']


def test_csentry_index(host):
    # This tests that traefik forwards traffic to the csentry web server
    # and that we can access the csentry login page
    cmd = host.command('curl -H Host:ics-ans-role-csentry-default -k -L https://localhost')
    assert '<title>Login - CSEntry</title>' in cmd.stdout


def test_root_crontab(host):
    with host.sudo():
        crontab = host.check_output('crontab -l')
    assert crontab == """#Ansible: Run flask maintenance every night
17 2 * * * /usr/bin/docker exec csentry_web flask maintenance > /tmp/flask_maintenance.log 2>&1
#Ansible: Run database dump
17 1 * * * /usr/local/sbin/dump-db > /tmp/dump-db.log 2>&1"""
