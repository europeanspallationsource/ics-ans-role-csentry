ics-ans-role-csentry
====================

Ansible role to install the CSEntry web application.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
csentry_processes: 4
csentry_settings: "{{ playbook_dir }}/config/settings.cfg"
csentry_network: csentry-network
csentry_postgres_tag: 9.6
csentry_postgres_user: ics
csentry_postgres_password: ics
csentry_postgres_db: csentry_db
csentry_postgres_data: /etc/csentry/data
csentry_image: registry.esss.lu.se/ics-infrastructure/csentry
csentry_tag: latest
csentry_hostname: "{{ ansible_fqdn }}"
csentry_uwsgi_buffer_size: 16384
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-csentry
```

License
-------

BSD 2-clause
